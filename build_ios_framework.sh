# # Configure for building only uadecore (to get the emulator source code generated)
if ./configure --only-uadecore && make; then
echo UADECORE BUILD SUCCEEDED
else
exit; #configure failed
fi

# Prepare UAE code for the static lib
if ./src/gencpu && ./src/build68k <src/table68k >cpudefs.c; then
echo UAE CPU GENERATION SUCCEEDED
else
exit; # no gencpu generated
fi

# CREATE RESOURCE BUNDLE
rm -rf UADERes.bundle
mkdir UADERes.bundle

cp eagleplayer.conf UADERes.bundle
cp uade.conf UADERES.bundle
cp uaerc UADERes.bundle
cp -r players UADERes.bundle
# drop the hippel-coso test script from bundle 
# (unsigned scripts bump on appstore review)
cp amigasrc/score/score UADERes.bundle

# Generate project file
xcodegen

# BUILD FOR HW & SIMULATOR
BUILD_DIR="."
CONFIGURATION="Release"
SCHEME_NAME="uade_ios"
FRAMEWORK_NAME="uade_ios"

SIMULATOR_ARCHIVE_PATH="${BUILD_DIR}/${CONFIGURATION}/${FRAMEWORK_NAME}-iphonesimulator.xcarchive"
DEVICE_ARCHIVE_PATH="${BUILD_DIR}/${CONFIGURATION}/${FRAMEWORK_NAME}-iphoneos.xcarchive"

xcodebuild clean -project uade_ios.xcodeproj/ -scheme "uade_ios"

xcodebuild archive \
  ONLY_ACTIVE_ARCH=NO \
  -scheme ${SCHEME_NAME} \
  -project "${SCHEME_NAME}.xcodeproj" \
  -archivePath ${SIMULATOR_ARCHIVE_PATH} \
  -sdk iphonesimulator \
  BUILD_LIBRARY_FOR_DISTRIBUTION=YES \
  SKIP_INSTALL=NO

xcodebuild archive \
  ONLY_ACTIVE_ARCH=NO \
  -scheme ${SCHEME_NAME} \
  -project "${SCHEME_NAME}.xcodeproj" \
  -archivePath ${DEVICE_ARCHIVE_PATH} \
  -sdk iphoneos \
  BUILD_LIBRARY_FOR_DISTRIBUTION=YES \
  SKIP_INSTALL=NO

# CREATE FAT FRAMEOWRK
rm -rf uade_ios.xcframework
xcodebuild -create-xcframework \
    -framework ${SIMULATOR_ARCHIVE_PATH}/Products/Library/Frameworks/uade_ios.framework \
    -framework ${DEVICE_ARCHIVE_PATH}/Products/Library/Frameworks/uade_ios.framework \
    -output uade_ios.xcframework

# CLEANUP 
rm -rf ${SIMULATOR_ARCHIVE_PATH}
rm -rf ${DEVICE_ARCHIVE_PATH}
